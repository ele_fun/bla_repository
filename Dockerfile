Arg GO_VERSION=1.21.0

From golang:${GO_VERSION}-alpine As builder

Workdir /app

Copy . .

Run apk add git bash --no-cache && \
    go env -w CGO_ENABLED=0 GOOS=linux GOARCH=amd64 && \
    go mod tidy && \
    go build -a -installsuffix cgo -ldflags '-w -extldflags "-static"' \
    -o /go/bin/main cmd/*.go

From alpine:latest

Workdir /app

Copy --from=builder /go/bin/main /app/main
Copy .env /app/.env

Cmd ["./main"]
