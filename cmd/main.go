package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Elevarup/ele_fun/bla_repository/internal/database/postgres"
	usershandler "gitlab.com/Elevarup/ele_fun/bla_repository/internal/handlers/users_handler"
	"gitlab.com/Elevarup/ele_fun/bla_repository/internal/pkg"
	dbrepository "gitlab.com/Elevarup/ele_fun/bla_repository/internal/repository/db_repository"
)

func main() {
	// TODO: load env
	if err := pkg.LoadEnv(); err != nil {
		log.Println("load env, error: ", err)
		os.Exit(0)
	}
	// TODO: init dbrepository
	p, err := postgres.New(pkg.Env.UrlDB)
	if err != nil {
		log.Println("init dbrepository, postgres, error: ", err)
		os.Exit(0)
	}
	dbrepository.Set(p)

	// TODO: init router
	router := initRouter()

	// TODO: init server
	server := http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf("0.0.0.0:%s", pkg.Env.Port),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println("listen and server, error: ", err)
		}
	}()

	// TODO: catch signal interrupt
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	// TODO: shutdown server
	ctx, cancel := context.WithTimeout(context.TODO(), 15*time.Second)
	defer cancel()

	server.Shutdown(ctx)

}

func initRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", usershandler.Handler.All).Methods(http.MethodGet)
	r.HandleFunc("/", usershandler.Handler.New).Methods(http.MethodPost)

	return r
}
