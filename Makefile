D=docker-compose


all: build run

build:
	$(D) build --no-cache

run:
	$(D) up -d 

down:
	$(D) down

logs:
	$(D) logs -f

tail:
	$(D) logs --tail=100
