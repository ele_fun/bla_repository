package dbrepository

import (
	"context"

	"gitlab.com/Elevarup/ele_fun/bla_repository/internal/models"
)

type (
	Repository interface {
		All(ctx context.Context) ([]*models.User, error)
		New(ctx context.Context, u models.User) error
		GetByID(ctx context.Context, u models.User) (*models.User, error)
		Close(ctx context.Context) error
	}
)

var implementation Repository

func Set(r Repository) { implementation = r }

func All(ctx context.Context) ([]*models.User, error) {
	return implementation.All(ctx)
}
func New(ctx context.Context, u models.User) error {
	return implementation.New(ctx, u)
}
func GetByID(ctx context.Context, u models.User) (*models.User, error) {
	return implementation.GetByID(ctx, u)
}
func Close(ctx context.Context) error {
	return implementation.Close(ctx)
}
