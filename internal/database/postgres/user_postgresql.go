package postgres

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq"
	"gitlab.com/Elevarup/ele_fun/bla_repository/internal/models"
)

type (
	Postgres struct {
		db *sql.DB
	}
)

func New(urlDB string) (*Postgres, error) {
	db, err := sql.Open("postgres", urlDB)
	if err != nil {
		return nil, err
	}

	return &Postgres{db}, nil
}

func (p *Postgres) All(ctx context.Context) ([]*models.User, error) {
	rows, err := p.db.QueryContext(ctx, "Select id, name, description From users;")
	defer rows.Close()

	if err != nil {
		return nil, err
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	users := []*models.User{}

	for rows.Next() {
		user := models.User{}
		if err := rows.Scan(&user.ID, &user.Name, &user.Descripton); err != nil {
			return nil, err
		}
		users = append(users, &user)
	}

	return users, nil
}
func (p *Postgres) New(ctx context.Context, u models.User) error {
	_, err := p.db.ExecContext(ctx, "Insert Into users (name, description) Values ($1, $2)", u.Name, u.Descripton)
	return err
}
func (p *Postgres) GetByID(ctx context.Context, u models.User) (*models.User, error) {
	row := p.db.QueryRowContext(ctx, "Select id, name, description From users Where id = $1", u.ID)
	if err := row.Err(); err != nil {
		return nil, err
	}

	user := &models.User{}
	if err := row.Scan(&user.ID, &user.Name, &user.Descripton); err != nil {
		return nil, err
	}
	return user, nil

}
func (p *Postgres) Close(ctx context.Context) error {
	return p.db.Close()
}
