package usershandler

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/Elevarup/ele_fun/bla_repository/internal/models"
	dbrepository "gitlab.com/Elevarup/ele_fun/bla_repository/internal/repository/db_repository"
)

type (
	handler  struct{}
	Response struct {
		Success bool   `json:"success"`
		Code    int    `json:"code"`
		Message string `json:"message"`
		Data    any    `json:"data,omitempty"`
	}
)

var Handler handler

func helper(w http.ResponseWriter, code int, data any) {
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(data)
}

func (h *handler) All(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	list, err := dbrepository.All(r.Context())
	if err != nil {
		helper(w, http.StatusInternalServerError, Response{
			Success: true,
			Code:    http.StatusInternalServerError,
			Message: "error",
		})
		return
	}

	helper(w, http.StatusOK, Response{
		Success: true,
		Code:    http.StatusOK,
		Message: "success",
		Data:    list,
	})
}

func (h *handler) New(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	body, err := io.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		helper(w, http.StatusBadRequest, Response{
			Success: true,
			Code:    http.StatusBadRequest,
			Message: "error body",
		})
		return
	}

	var user models.User
	if err := json.Unmarshal(body, &user); err != nil {
		helper(w, http.StatusInternalServerError, Response{
			Success: true,
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
		return
	}
	err = dbrepository.New(r.Context(), user)
	if err != nil {
		helper(w, http.StatusInternalServerError, Response{
			Success: true,
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
		return
	}
	helper(w, http.StatusCreated, Response{
		Success: true,
		Code:    http.StatusCreated,
		Message: "created",
	})
}
