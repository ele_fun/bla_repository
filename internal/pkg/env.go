package pkg

import "github.com/ilyakaznacheev/cleanenv"

type (
	env struct {
		UrlDB string `env:"url_db" env-required:"true"`
		Port  string `env:"port" env-default:"3003"`
	}
)

var Env env

func LoadEnv() error {
	return cleanenv.ReadConfig(".env", &Env)
}
